﻿using Atlas.UI.Systems;
using Concord.Infrastructure.Messages.Discord;
using Concord.Services.Interfaces;
using Concord.View.Windows;
using Concord.ViewModel.Controls;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Window = Atlas.UI.Windows.Window;

namespace Concord.ViewModel.Windows
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Title { get; }
        
        public List<ServerItemViewModel> Servers { get; private set; }
        public List<DirectMessageItemViewModel> DirectMessageChannels { get; private set; }

        private IDiscordService DiscordService { get; }

        public MainWindowViewModel()
        {
            Title = "Concord";
            Servers = new List<ServerItemViewModel>();
            
            Messenger.Default.Register<UpdateGuildList>(this, OnUpdateGuildListReceived);
            Messenger.Default.Register<UpdateDirectMessageChannelList>(this, OnUpdateDirectMessageChannelListReceived);

            DiscordService = GetService<IDiscordService>();
        }

        [AsyncCommand]
        public async Task FemaleBoxer()
        {
            await DiscordService.LogIn();
        }

        [Command]
        public void OpenSettingsWindow(Window owner)
        {
            SingleInstanceWindowManager.OpenOrActivateDialog<SettingsWindow>(owner, WindowStartupLocation.CenterOwner);
        }

        public void OnUpdateGuildListReceived(UpdateGuildList message)
        {
            Servers = new List<ServerItemViewModel>(message.Guilds.Select(x => new ServerItemViewModel(x.Name, x.IconUrl)));
        }

        private void OnUpdateDirectMessageChannelListReceived(UpdateDirectMessageChannelList message)
        {
            DirectMessageChannels = new List<DirectMessageItemViewModel>(message.Channels.Select(x => new DirectMessageItemViewModel(x)));
        }
    }
}