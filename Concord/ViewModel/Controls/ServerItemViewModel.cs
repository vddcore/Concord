﻿using System.Threading.Tasks;

namespace Concord.ViewModel.Controls
{
    public class ServerItemViewModel : ChatItemViewModelBase
    {
        public string ServerName { get; }

        public ServerItemViewModel()
        {
            Log.Warn("Initialized with empty constructor! This is not supported and will be removed in the future.");
            
            ServerName = "Yeet.";

            Task.Run(async () =>
            {
                await DownloadServerImageAsync(
                    "https://cdn.vox-cdn.com/thumbor/vWBtbWAY2Oqd_FbznQfvi9ZHzk0=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/10838139/gachigasm.jpg");
            });
        }

        public ServerItemViewModel(string serverName, string iconUrl)
        {
            ServerName = serverName;
            Task.Run(async () => { await DownloadServerImageAsync(iconUrl); });
        }
    }
}