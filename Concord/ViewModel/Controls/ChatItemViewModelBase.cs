﻿using Concord.Resources;
using DevExpress.Mvvm;
using NLog;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Concord.ViewModel.Controls
{
    public class ChatItemViewModelBase : ViewModelBase
    {
        protected Logger Log => LogManager.GetCurrentClassLogger();

        public bool IsLoading { get; protected set; }

        public byte[] Icon { get; protected set; }
        public int IconWidth { get; protected set; } = 32;
        public int IconHeight { get; protected set; } = 32;

        protected async Task DownloadServerImageAsync(string imageLink)
        {
            IsLoading = true;
            Icon = null;

            using var client = new WebClient();

            try
            {
                Icon = await client.DownloadDataTaskAsync(imageLink);
            }
            catch (Exception e)
            {
                Log.Warn(e, "Failed to fetch image. Defaulting to question mark.");

                using var stream = new MemoryStream();
                Images.QuestionMark.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                Icon = stream.ToArray();
                IconWidth = IconHeight = 16;
            }

            IsLoading = false;
        }
    }
}
