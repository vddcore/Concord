﻿using System;
using System.Collections.Generic;
using Concord.Infrastructure.Configuration.Core;

namespace Concord.Infrastructure.Configuration
{
    public class ConfigManager
    {
        private static readonly Lazy<ConfigManager> LazyInitializer = new Lazy<ConfigManager>(() => new ConfigManager(), true);
        private Dictionary<Type, Settings> Configs { get; }

        public static ConfigManager Instance => LazyInitializer.Value;

        private ConfigManager()
        {
            Configs = new Dictionary<Type, Settings>();
        }

        public Settings Create<T>(string name) where T : ConfigShard
            => Create(typeof(T), name);

        public Settings Create(Type type, string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException($"Name parameter cannot be null nor empty when creating a config.");
            
            if (typeof(ConfigShard).IsAssignableFrom(type))
            {
                var settings = new Settings(name);
                Configs.Add(type, settings);

                return settings;
            }
            
            throw new InvalidOperationException($"Tried to create a config object not deriving from {nameof(ConfigShard)}.");
        }

        public Settings Get<T>() where T: ConfigShard
            => Get(typeof(T));

        public Settings Get(Type type)
        {
            if (Configs.ContainsKey(type))
                return Configs[type];

            return null;
        }
    }
}