﻿using System;
using AutoProperties;
using Concord.Infrastructure.Events;
using Newtonsoft.Json;
using NLog;

namespace Concord.Infrastructure.Configuration
{
    public class ConfigShard
    {
        [JsonIgnore]
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [JsonIgnore]
        [InterceptIgnore]
        public string Name { get; }

        [JsonIgnore]
        [InterceptIgnore]
        public Type Type { get; }

        public event EventHandler<ShardPropertyChangedEventArgs> PropertyChanged;

        protected ConfigShard(Type type, string name)
        {
            Name = name;
            Type = type;

            var shard = ConfigManager.Instance.Get(type);

            if (shard == null)
            {
                ConfigManager.Instance.Create(type, name);
                Log.Info($"1st time constructed shard for type '{type.Name}', config named '{name}.json'...");
            }
        }

        [SetInterceptor]
        public void InterceptSetEvent(string propertyName, object value)
        {
            var shardSettings = ConfigManager.Instance.Get(Type);

            if (shardSettings != null)
            {
                shardSettings[propertyName] = value?.ToString();
                shardSettings.Save().GetAwaiter().GetResult();
                
                PropertyChanged?.Invoke(this, new ShardPropertyChangedEventArgs(propertyName, value));
            }
        }

        [GetInterceptor]
        public object InterceptGetEvent(string propertyName)
        {
            var shardSettings = ConfigManager.Instance.Get(Type);
            return shardSettings?[propertyName];
        }
    }
}