﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Concord.Infrastructure.Configuration.Core
{
    public class Settings : Section
    {
        private static Logger Log => LogManager.GetCurrentClassLogger();

        private string FileName { get; }
        private string RootDirectory { get; }
        private string SettingsDirectory => Path.Combine(RootDirectory, "Settings");

        public string FilePath => Path.Combine(SettingsDirectory, FileName);

        public Settings(string name)
        {
            RootDirectory = Path.GetDirectoryName(Assembly.GetCallingAssembly().Location);

            FileName = $"{name}.json";

            if (File.Exists(FilePath))
            {
                using var sr = new StreamReader(FilePath);
                var json = sr.ReadToEnd();

                try
                {
                    var sec = JsonConvert.DeserializeObject<Section>(json);

                    foreach (var k in sec.Keys)
                        Add(k, sec[k]);
                }
                catch (JsonException je)
                {
                    Log.Error(je);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }

            Dirty = false;
        }

        public async Task SaveIfDirty(bool formatJson = true)
        {
            if (Dirty)
                await Save(formatJson);
        }

        public async Task Save(bool formatJson = true)
        {
            if (!Directory.Exists(SettingsDirectory))
                Directory.CreateDirectory(SettingsDirectory);

            try
            {
                using (var sw = new StreamWriter(FilePath, false))
                {
                    await sw.WriteLineAsync(JsonConvert.SerializeObject(this, formatJson ? Formatting.Indented : Formatting.None));
                }

                Dirty = false;
            }
            catch (JsonException je)
            {
                Log.Error(je);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}