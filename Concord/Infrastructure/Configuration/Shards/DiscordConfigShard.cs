﻿namespace Concord.Infrastructure.Configuration.Shards
{
    public class DiscordConfigShard : ConfigShard
    {
        public string Token { get; set; }

        public DiscordConfigShard() : base(typeof(DiscordConfigShard), "discord")
        {
        }
    }
}