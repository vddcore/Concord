﻿using Discord;

namespace Concord.Infrastructure.Messages.Discord
{
    public class UpdateUser
    {
        public IUser Before { get; }
        public IUser After { get; }

        public UpdateUser(IUser before, IUser after)
        {
            Before = before;
            After = after;
        }
    }
}
