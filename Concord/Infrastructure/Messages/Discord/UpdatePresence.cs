﻿using Discord;
using System.Collections.Generic;

namespace Concord.Infrastructure.Messages.Discord
{
    public class UpdatePresence
    {
        public IPresence Before { get; }

        public Game Game { get; }
        public IUser User { get; }
        public IGuild Guild { get; }
        public List<IRole> Roles { get; }

        public string Status { get; }

        public UpdatePresence(IPresence before, Game game, IUser user, IGuild guild, List<IRole> roles, string status)
        {
            Before = before;

            Game = game;
            User = user;
            Guild = guild;
            Roles = roles;
            
            Status = status;
        }

    }
}
