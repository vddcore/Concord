﻿using Concord.Services;
using Concord.WindowsInterop;
using DevExpress.Mvvm;
using System.Windows;

namespace Concord
{
    public partial class App : Application
    {
        private static DiscordService _discordService;

        protected override void OnStartup(StartupEventArgs e)
        {
            Kernel32.AttachConsole();

            Messenger.Default = new Messenger(false);

            ServiceContainer.Default = new ServiceContainer(this);
            ViewModelLocator.Default = new ViewModelLocator(this);

            _discordService = new DiscordService(false);
        }
    }
}
