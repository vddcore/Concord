﻿using Concord.Infrastructure.Mvvm.Interfaces;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Concord.Services.Interfaces
{
    public interface IDiscordService : IService
    {
        bool AutoLogin { get; }
        bool IsLoggedIn { get; }

        DiscordSocketClient WebSocketClient { get; }
        DiscordRestClient RestClient { get; }

        Task LogIn();

        Task<List<IGuild>> FetchAvailableGuilds();
        Task<List<IGuildChannel>> FetchChannelsFor(IGuild guild);
    }
}