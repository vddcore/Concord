﻿using Concord.Infrastructure.Configuration.Shards;
using Concord.Infrastructure.Messages.Discord;
using Concord.Services.Interfaces;
using DevExpress.Mvvm;
using Discord;
using Discord.Rest;
using Discord.WebSocket;
using NLog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Concord.Services
{
    public class DiscordService : IDiscordService
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private DiscordSocketConfig SocketConfig { get; }

        public DiscordSocketClient WebSocketClient { get; }
        public DiscordRestClient RestClient => WebSocketClient.Rest;

        public DiscordConfigShard DiscordConfig { get; }

        public bool AutoLogin { get; }
        public bool IsLoggedIn { get; private set; }

        public DiscordService(bool autoLogin)
        {
            AutoLogin = autoLogin;

            DiscordConfig = new DiscordConfigShard();

            SocketConfig = new DiscordSocketConfig
            {
                AlwaysDownloadUsers = true,
                LogLevel = LogSeverity.Debug
            };

            WebSocketClient = new DiscordSocketClient(SocketConfig);
            WebSocketClient.Ready += WebSocketClient_Ready;
            WebSocketClient.Log += WebSocketClient_Log;
            WebSocketClient.LoggedIn += WebSocketClient_LoggedIn;

            if (AutoLogin)
            {
                Log.Warn("Autologin requested. Doing so...");
                Task.Run(async () => await LogIn());
            }

            ServiceContainer.Default.RegisterService(this);
        }

        public async Task LogIn()
        {
            await WebSocketClient.LoginAsync(TokenType.User, DiscordConfig.Token, false);
        }

        public async Task<List<IGuild>> FetchAvailableGuilds()
        {
            Log.Info("Fetching guilds...");
            return new List<IGuild>(await WebSocketClient.Rest.GetGuildsAsync());
        }

        public async Task<List<IGuildChannel>> FetchChannelsFor(IGuild guild)
        {
            return (await guild.GetChannelsAsync()).ToList();
        }

        public async Task<List<IDMChannel>> FetchDirectMessageChannels()
        {
            Log.Info("Fetching DMs...");
            return new List<IDMChannel>(await WebSocketClient.Rest.GetDMChannelsAsync());
        }

        private async Task WebSocketClient_Log(LogMessage msg)
        {
            switch (msg.Severity)
            {
                case LogSeverity.Critical:
                case LogSeverity.Error:
                    Log.Error(msg.Message);
                    break;

                case LogSeverity.Warning:
                    Log.Warn(msg.Message);
                    break;

                case LogSeverity.Debug:
                    Log.Debug(msg.Message);
                    break;

                default: Log.Info(msg.Message); break;
            }

            await Task.CompletedTask;
        }

        private async Task WebSocketClient_Ready()
        {
            Log.Info("WebSocketClient_Ready");
            await Task.CompletedTask;
        }

        private async Task WebSocketClient_LoggedIn()
        {
            IsLoggedIn = true;

            var guilds = await FetchAvailableGuilds();
            Messenger.Default.Send(
                new UpdateGuildList(guilds)
            );

            var dmChannels = await FetchDirectMessageChannels();
            Messenger.Default.Send(
                new UpdateDirectMessageChannelList(dmChannels)
            );
        }
    }
}