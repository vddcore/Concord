﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Concord.WindowsInterop
{
    public static class Kernel32
    {
        private static bool _consoleAttached;

        private static TextWriter _originalConOut = Console.Out;
        private static StreamWriter _consoleWriter;

        [DllImport("kernel32.dll")]
        internal static extern bool AllocConsole();

        [DllImport("kernel32.dll")]
        internal static extern bool FreeConsole();

        public static void AttachConsole()
        {
            if (!_consoleAttached)
            {
                if (AllocConsole())
                {
                    _consoleWriter = new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true };
                    Console.SetOut(_consoleWriter);

                    _consoleAttached = true;
                }
            }
        }

        public static void DetachConsole()
        {
            if (_consoleAttached)
            {
                _consoleWriter.Dispose();
                Console.SetOut(_originalConOut);

                FreeConsole();
                _consoleAttached = false;
            }
        }
    }
}