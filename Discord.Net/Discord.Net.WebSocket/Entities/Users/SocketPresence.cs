using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using Model = Discord.API.Presence;

namespace Discord.WebSocket
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public struct SocketPresence : IPresence
    {
        public UserStatus Status { get; }
        public IActivity Activity { get; }
        
        public IImmutableSet<ClientType> ActiveClients { get; }
        internal SocketPresence(UserStatus status, IActivity activity, IImmutableSet<ClientType> activeClients)
        {
            Status = status;
            Activity= activity;
            ActiveClients = activeClients;
        }
        
        internal static SocketPresence Create(Model model)
        {
            var clients = ConvertClientTypesDict(model.ClientStatus.GetValueOrDefault());
            return new SocketPresence(model.Status, model.Game?.ToEntity(), clients);
        }
        
        private static IImmutableSet<ClientType> ConvertClientTypesDict(IDictionary<string, string> clientTypesDict)
        {
            if (clientTypesDict == null || clientTypesDict.Count == 0)
                return ImmutableHashSet<ClientType>.Empty;
            var set = new HashSet<ClientType>();
            foreach (var key in clientTypesDict.Keys)
            {
                if (Enum.TryParse(key, true, out ClientType type))
                    set.Add(type);
                // quietly discard ClientTypes that do not match
            }
            return set.ToImmutableHashSet();
        }

        public override string ToString() => Status.ToString();
        private string DebuggerDisplay => $"{Status}{(Activity != null ? $", {Activity.Name}": "")}";

        internal SocketPresence Clone() => this;
    }
}
