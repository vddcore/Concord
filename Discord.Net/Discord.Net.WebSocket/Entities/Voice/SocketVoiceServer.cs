using System.Diagnostics;

namespace Discord.WebSocket
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public class SocketVoiceServer
    {
        public Cacheable<IGuild, ulong> Guild { get; }

        public string Endpoint { get; }
        public string Token { get; }

        internal SocketVoiceServer(Cacheable<IGuild, ulong> guild, string endpoint, string token)
        {
            Guild = guild;
            Endpoint = endpoint;
            Token = token;
        }

        private string DebuggerDisplay => $"SocketVoiceServer ({Guild.Id})";
    }
}
