﻿using System;
using System.Threading.Tasks;

namespace Discord.WebSocket
{
    public partial class DiscordShardedClient
    {
        private readonly AsyncEvent<Func<DiscordSocketClient, Task>> _shardConnectedEvent = new AsyncEvent<Func<DiscordSocketClient, Task>>();
        private readonly AsyncEvent<Func<Exception, DiscordSocketClient, Task>> _shardDisconnectedEvent = new AsyncEvent<Func<Exception, DiscordSocketClient, Task>>();
        private readonly AsyncEvent<Func<DiscordSocketClient, Task>> _shardReadyEvent = new AsyncEvent<Func<DiscordSocketClient, Task>>();
        private readonly AsyncEvent<Func<int, int, DiscordSocketClient, Task>> _shardLatencyUpdatedEvent = new AsyncEvent<Func<int, int, DiscordSocketClient, Task>>();
        
        public event Func<DiscordSocketClient, Task> ShardConnected 
        {
            add { _shardConnectedEvent.Add(value); }
            remove { _shardConnectedEvent.Remove(value); }
        }
        
        public event Func<Exception, DiscordSocketClient, Task> ShardDisconnected 
        {
            add { _shardDisconnectedEvent.Add(value); }
            remove { _shardDisconnectedEvent.Remove(value); }
        }
        
        public event Func<DiscordSocketClient, Task> ShardReady 
        {
            add { _shardReadyEvent.Add(value); }
            remove { _shardReadyEvent.Remove(value); }
        }
        
        public event Func<int, int, DiscordSocketClient, Task> ShardLatencyUpdated 
        {
            add { _shardLatencyUpdatedEvent.Add(value); }
            remove { _shardLatencyUpdatedEvent.Remove(value); }
        }
    }
}