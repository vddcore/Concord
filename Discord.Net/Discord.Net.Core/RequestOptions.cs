using System.Threading;

namespace Discord
{
    public class RequestOptions
    {
        public static RequestOptions Default => new RequestOptions();

        public int? Timeout { get; set; }

        public CancellationToken CancelToken { get; set; } = CancellationToken.None;

        public RetryMode? RetryMode { get; set; }
        public bool HeaderOnly { get; internal set; }

        public string AuditLogReason { get; set; }
        public bool? UseSystemClock { get; set; }

        internal string BucketId { get; set; }
        internal bool IsClientBucket { get; set; }
        internal bool IsReactionBucket { get; set; }

        internal bool IgnoreState { get; set; }

        internal static RequestOptions CreateOrClone(RequestOptions options)
        {
            if (options == null)
                return new RequestOptions();
            else
                return options.Clone();
        }

        public RequestOptions()
        {
            Timeout = DiscordConfig.DefaultRequestTimeout;
        }

        public RequestOptions Clone() => MemberwiseClone() as RequestOptions;
    }
}
