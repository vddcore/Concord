namespace Discord.Commands
{
    public interface ICommandContext
    {
        IDiscordClient Client { get; }

        IUser User { get; }
        IGuild Guild { get; }
        IUserMessage Message { get; }
        IMessageChannel Channel { get; }
    }
}
