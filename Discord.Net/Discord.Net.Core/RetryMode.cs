using System;

namespace Discord
{
    [Flags]
    public enum RetryMode
    {
        AlwaysFail = 0x0,
        RetryTimeouts = 0x1,

        // RetryErrors = 0x2,
        // FIXME: Figure out why RetryErrors is commented out.

        RetryRatelimit = 0x4,
        Retry502 = 0x8,
        AlwaysRetry = RetryTimeouts | /*RetryErrors |*/ RetryRatelimit | Retry502,
    }
}
