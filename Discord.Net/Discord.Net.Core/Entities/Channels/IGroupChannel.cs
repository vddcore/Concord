using System.Threading.Tasks;

namespace Discord
{
    public interface IGroupChannel : IMessageChannel, IPrivateChannel, IAudioChannel
    {
        Task LeaveAsync(RequestOptions options = null);
    }
}
