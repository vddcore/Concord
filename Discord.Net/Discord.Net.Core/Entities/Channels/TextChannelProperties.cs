using System;

namespace Discord
{
    public class TextChannelProperties : GuildChannelProperties
    {
        public Optional<string> Topic { get; set; }
        public Optional<bool> IsNsfw { get; set; }
        public Optional<int> SlowModeInterval { get; set; }
    }
}
