namespace Discord
{
    public class GuildChannelProperties
    {
        public Optional<string> Name { get; set; }
        public Optional<int> Position { get; set; }
        public Optional<ulong?> CategoryId { get; set; }
    }
}
