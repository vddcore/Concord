using Discord.Audio;
using System.Threading.Tasks;

namespace Discord
{
    public interface IAudioChannel : IChannel
    {
        Task<IAudioClient> ConnectAsync(bool selfDeaf = false, bool selfMute = false, bool external = false);
        Task DisconnectAsync();
    }
}
