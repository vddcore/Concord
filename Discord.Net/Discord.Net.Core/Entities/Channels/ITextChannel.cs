using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Discord
{
    public interface ITextChannel : IMessageChannel, IMentionable, INestedChannel
    {
        bool IsNsfw { get; }
        string Topic { get; }
        int SlowModeInterval { get; }

        Task DeleteMessagesAsync(IEnumerable<IMessage> messages, RequestOptions options = null);
        Task DeleteMessagesAsync(IEnumerable<ulong> messageIds, RequestOptions options = null);
        Task ModifyAsync(Action<TextChannelProperties> func, RequestOptions options = null);

        Task<IWebhook> CreateWebhookAsync(string name, Stream avatar = null, RequestOptions options = null);
        Task<IWebhook> GetWebhookAsync(ulong id, RequestOptions options = null);
        Task<IReadOnlyCollection<IWebhook>> GetWebhooksAsync(RequestOptions options = null);
    }
}
