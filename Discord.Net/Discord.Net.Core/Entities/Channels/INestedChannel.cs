using System.Collections.Generic;
using System.Threading.Tasks;

namespace Discord
{
    public interface INestedChannel : IGuildChannel
    {
        ulong? CategoryId { get; }

        Task<ICategoryChannel> GetCategoryAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task SyncPermissionsAsync(RequestOptions options = null);
        Task<IInviteMetadata> CreateInviteAsync(int? maxAge = 86400, int? maxUses = default(int?), bool isTemporary = false, bool isUnique = false, RequestOptions options = null);
        Task<IReadOnlyCollection<IInviteMetadata>> GetInvitesAsync(RequestOptions options = null);
    }
}
