using System;
using System.Threading.Tasks;

namespace Discord
{
    public interface IVoiceChannel : INestedChannel, IAudioChannel
    {
        int Bitrate { get; }
        int? UserLimit { get; }

        Task ModifyAsync(Action<VoiceChannelProperties> func, RequestOptions options = null);
    }
}
