using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Discord
{
    public interface IGuildChannel : IChannel, IDeletable
    {
        int Position { get; }

        IGuild Guild { get; }
        ulong GuildId { get; }
        IReadOnlyCollection<Overwrite> PermissionOverwrites { get; }
        
        OverwritePermissions? GetPermissionOverwrite(IRole role);
        OverwritePermissions? GetPermissionOverwrite(IUser user);

        Task ModifyAsync(Action<GuildChannelProperties> func, RequestOptions options = null);
        Task RemovePermissionOverwriteAsync(IRole role, RequestOptions options = null);
        Task RemovePermissionOverwriteAsync(IUser user, RequestOptions options = null);

        Task AddPermissionOverwriteAsync(IRole role, OverwritePermissions permissions, RequestOptions options = null);
        Task AddPermissionOverwriteAsync(IUser user, OverwritePermissions permissions, RequestOptions options = null);

        new IAsyncEnumerable<IReadOnlyCollection<IGuildUser>> GetUsersAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        new Task<IGuildUser> GetUserAsync(ulong id, CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
    }
}
