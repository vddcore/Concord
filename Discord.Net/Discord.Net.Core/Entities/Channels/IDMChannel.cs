using System.Threading.Tasks;

namespace Discord
{
    public interface IDMChannel : IMessageChannel, IPrivateChannel
    {
        IUser Recipient { get; }
        Task CloseAsync(RequestOptions options = null);
    }
}
