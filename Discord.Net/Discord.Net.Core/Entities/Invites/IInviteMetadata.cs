using System;

namespace Discord
{
    public interface IInviteMetadata : IInvite
    {
        IUser Inviter { get; }

        bool IsRevoked { get; }
        bool IsTemporary { get; }
        
        int? MaxAge { get; }
        int? MaxUses { get; }
        int? Uses { get; }
        
        DateTimeOffset? CreatedAt { get; }
    }
}
