using System.Diagnostics;

namespace Discord
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public class MessageActivity
    {
        public MessageActivityType Type { get; internal set; }
        public string PartyId { get; internal set; }

        private string DebuggerDisplay
            => $"{Type}{(string.IsNullOrWhiteSpace(PartyId) ? "" : $" {PartyId}")}";

        public override string ToString() => DebuggerDisplay;
    }
}
