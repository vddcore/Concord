namespace Discord
{
    public interface IAttachment
    {
        ulong Id { get; }

        string Filename { get; }
        
        string ProxyUrl { get; }
        string Url { get; }

        int Size { get; }
        int? Height { get; }
        int? Width { get; }
    }
}
