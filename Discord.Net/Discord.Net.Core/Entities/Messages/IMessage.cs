using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Discord
{
    public interface IMessage : ISnowflakeEntity, IDeletable
    {
        MessageType Type { get; }
        MessageSource Source { get; }

        bool IsTTS { get; }
        bool IsPinned { get; }
        bool IsSuppressed { get; }
        
        string Content { get; }
        
        DateTimeOffset Timestamp { get; }
        DateTimeOffset? EditedTimestamp { get; }
        
        IMessageChannel Channel { get; }
        IUser Author { get; }
        
        IReadOnlyCollection<IAttachment> Attachments { get; }
        IReadOnlyCollection<IEmbed> Embeds { get; }
        IReadOnlyCollection<ITag> Tags { get; }
        
        IReadOnlyCollection<ulong> MentionedChannelIds { get; }
        IReadOnlyCollection<ulong> MentionedRoleIds { get; }
        IReadOnlyCollection<ulong> MentionedUserIds { get; }

        MessageActivity Activity { get; }
        MessageApplication Application { get; }
        MessageReference Reference { get; }

        IReadOnlyDictionary<IEmote, ReactionMetadata> Reactions { get; }
        
        Task AddReactionAsync(IEmote emote, RequestOptions options = null);
        Task RemoveReactionAsync(IEmote emote, IUser user, RequestOptions options = null);
        Task RemoveReactionAsync(IEmote emote, ulong userId, RequestOptions options = null);
        Task RemoveAllReactionsAsync(RequestOptions options = null);
        
        IAsyncEnumerable<IReadOnlyCollection<IUser>> GetReactionUsersAsync(IEmote emoji, int limit, RequestOptions options = null);
    }
}
