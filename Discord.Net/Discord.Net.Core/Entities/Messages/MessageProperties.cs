namespace Discord
{
    public class MessageProperties
    {
        public Optional<string> Content { get; set; }
        public Optional<Embed> Embed { get; set; }
    }
}
