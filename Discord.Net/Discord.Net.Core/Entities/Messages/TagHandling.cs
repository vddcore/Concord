namespace Discord
{
    public enum TagHandling
    {
        Ignore = 0,
        Remove,
        Name,
        NameNoPrefix,
        FullName,
        FullNameNoPrefix,
        Sanitize
    }
}
