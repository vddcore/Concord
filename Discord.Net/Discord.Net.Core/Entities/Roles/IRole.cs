using System;
using System.Threading.Tasks;

namespace Discord
{
    public interface IRole : ISnowflakeEntity, IDeletable, IMentionable, IComparable<IRole>
    {
        IGuild Guild { get; }
        Color Color { get; }
        bool IsHoisted { get; }
        bool IsManaged { get; }
        bool IsMentionable { get; }
        string Name { get; }
        GuildPermissions Permissions { get; }
        int Position { get; }

        Task ModifyAsync(Action<RoleProperties> func, RequestOptions options = null);
    }
}
