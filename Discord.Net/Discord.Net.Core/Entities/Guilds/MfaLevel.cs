namespace Discord
{
    public enum MfaLevel
    {
        Disabled = 0,
        Enabled = 1
    }
}
