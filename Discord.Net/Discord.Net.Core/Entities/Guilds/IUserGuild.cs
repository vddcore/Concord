namespace Discord
{
    public interface IUserGuild : IDeletable, ISnowflakeEntity
    {
        string Name { get; }
        string IconUrl { get; }

        bool IsOwner { get; }
        GuildPermissions Permissions { get; }
    }
}
