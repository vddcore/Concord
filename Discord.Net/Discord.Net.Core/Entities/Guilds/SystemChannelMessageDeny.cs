using System;

namespace Discord
{
    [Flags]
    public enum SystemChannelMessageDeny
    {
        None = 0,
        WelcomeMessage = 1,
        GuildBoost = 2
    }
}
