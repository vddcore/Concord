namespace Discord
{
    public enum VerificationLevel
    {
        None = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        Extreme = 4
    }
}
