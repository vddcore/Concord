using Discord.Audio;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Discord
{
    public interface IGuild : IDeletable, ISnowflakeEntity
    {
        string Name { get; }

        int AFKTimeout { get; }
        bool IsEmbeddable { get; }
        
        DefaultMessageNotifications DefaultMessageNotifications { get; }
        MfaLevel MfaLevel { get; }
        VerificationLevel VerificationLevel { get; }
        ExplicitContentFilterLevel ExplicitContentFilter { get; }
        
        string IconId { get; }
        string IconUrl { get; }
        string SplashId { get; }
        string SplashUrl { get; }
        
        bool Available { get; }

        ulong? AFKChannelId { get; }
        ulong? EmbedChannelId { get; }
        ulong? SystemChannelId { get; }
        ulong OwnerId { get; }
        ulong? ApplicationId { get; }
        
        string VoiceRegionId { get; }
        
        IAudioClient AudioClient { get; }
        IRole EveryoneRole { get; }
        IReadOnlyCollection<GuildEmote> Emotes { get; }
        IReadOnlyCollection<string> Features { get; }
        IReadOnlyCollection<IRole> Roles { get; }

        PremiumTier PremiumTier { get; }

        string BannerId { get; }
        string BannerUrl { get; }

        string VanityURLCode { get; }

        SystemChannelMessageDeny SystemChannelFlags { get; }

        string Description { get; }
        int PremiumSubscriptionCount { get; }
        string PreferredLocale { get; }

        CultureInfo PreferredCulture { get; }

        Task ModifyAsync(Action<GuildProperties> func, RequestOptions options = null);
        Task ModifyEmbedAsync(Action<GuildEmbedProperties> func, RequestOptions options = null);
        Task ReorderChannelsAsync(IEnumerable<ReorderChannelProperties> args, RequestOptions options = null);
        Task ReorderRolesAsync(IEnumerable<ReorderRoleProperties> args, RequestOptions options = null);
        Task LeaveAsync(RequestOptions options = null);
        Task<IReadOnlyCollection<IBan>> GetBansAsync(RequestOptions options = null);
        
        Task<IBan> GetBanAsync(IUser user, RequestOptions options = null);
        Task<IBan> GetBanAsync(ulong userId, RequestOptions options = null);
        Task AddBanAsync(IUser user, int pruneDays = 0, string reason = null, RequestOptions options = null);
        Task AddBanAsync(ulong userId, int pruneDays = 0, string reason = null, RequestOptions options = null);
        Task RemoveBanAsync(IUser user, RequestOptions options = null);
        Task RemoveBanAsync(ulong userId, RequestOptions options = null);

        Task<IReadOnlyCollection<IGuildChannel>> GetChannelsAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IGuildChannel> GetChannelAsync(ulong id, CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IReadOnlyCollection<ITextChannel>> GetTextChannelsAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<ITextChannel> GetTextChannelAsync(ulong id, CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IReadOnlyCollection<IVoiceChannel>> GetVoiceChannelsAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IReadOnlyCollection<ICategoryChannel>> GetCategoriesAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IVoiceChannel> GetVoiceChannelAsync(ulong id, CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IVoiceChannel> GetAFKChannelAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<ITextChannel> GetSystemChannelAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<ITextChannel> GetDefaultChannelAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IGuildChannel> GetEmbedChannelAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<ITextChannel> CreateTextChannelAsync(string name, Action<TextChannelProperties> func = null, RequestOptions options = null);
        Task<IVoiceChannel> CreateVoiceChannelAsync(string name, Action<VoiceChannelProperties> func = null, RequestOptions options = null);
        Task<ICategoryChannel> CreateCategoryAsync(string name, Action<GuildChannelProperties> func = null, RequestOptions options = null);
        Task<IReadOnlyCollection<IVoiceRegion>> GetVoiceRegionsAsync(RequestOptions options = null);

        Task<IReadOnlyCollection<IGuildIntegration>> GetIntegrationsAsync(RequestOptions options = null);
        Task<IGuildIntegration> CreateIntegrationAsync(ulong id, string type, RequestOptions options = null);

        Task<IReadOnlyCollection<IInviteMetadata>> GetInvitesAsync(RequestOptions options = null);
        Task<IInviteMetadata> GetVanityInviteAsync(RequestOptions options = null);

        IRole GetRole(ulong id);

        Task<IRole> CreateRoleAsync(string name, GuildPermissions? permissions = null, Color? color = null, bool isHoisted = false, bool isMentionable = false, RequestOptions options = null);
        Task<IGuildUser> AddGuildUserAsync(ulong userId, string accessToken, Action<AddGuildUserProperties> func = null, RequestOptions options = null);
        Task<IReadOnlyCollection<IGuildUser>> GetUsersAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IGuildUser> GetUserAsync(ulong id, CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IGuildUser> GetCurrentUserAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);
        Task<IGuildUser> GetOwnerAsync(CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null);

        Task DownloadUsersAsync();
        Task<int> PruneUsersAsync(int days = 30, bool simulate = false, RequestOptions options = null);

        Task<IReadOnlyCollection<IAuditLogEntry>> GetAuditLogsAsync(int limit = DiscordConfig.MaxAuditLogEntriesPerBatch,
            CacheMode mode = CacheMode.AllowDownload, RequestOptions options = null, ulong? beforeId = null, ulong? userId = null,
            ActionType? actionType = null);

        Task<IWebhook> GetWebhookAsync(ulong id, RequestOptions options = null);
        Task<IReadOnlyCollection<IWebhook>> GetWebhooksAsync(RequestOptions options = null);

        Task<GuildEmote> GetEmoteAsync(ulong id, RequestOptions options = null);
        Task<GuildEmote> CreateEmoteAsync(string name, Image image, Optional<IEnumerable<IRole>> roles = default(Optional<IEnumerable<IRole>>), RequestOptions options = null);
        Task<GuildEmote> ModifyEmoteAsync(GuildEmote emote, Action<EmoteProperties> func, RequestOptions options = null);
        Task DeleteEmoteAsync(GuildEmote emote, RequestOptions options = null);
    }
}
