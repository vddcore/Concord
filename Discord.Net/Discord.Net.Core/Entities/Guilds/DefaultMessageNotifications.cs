namespace Discord
{
    public enum DefaultMessageNotifications
    {
        AllMessages = 0,
        MentionsOnly = 1
    }
}
