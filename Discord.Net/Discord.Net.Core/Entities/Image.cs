using System;
using System.IO;

namespace Discord
{
    public struct Image : IDisposable
    {
        private bool _isDisposed;

#pragma warning disable IDISP008
        public Stream Stream { get; }
#pragma warning restore IDISP008
        
        public Image(Stream stream)
        {
            _isDisposed = false;
            Stream = stream;
        }

        public Image(string path)
        {
            _isDisposed = false;
            Stream = File.OpenRead(path);
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if (!_isDisposed)
            {
#pragma warning disable IDISP007
                Stream?.Dispose();
#pragma warning restore IDISP007

                _isDisposed = true;
            }
        }
    }
}
