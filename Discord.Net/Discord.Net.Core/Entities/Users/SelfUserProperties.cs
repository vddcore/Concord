namespace Discord
{
    public class SelfUserProperties
    {
        public Optional<string> Username { get; set; }
        public Optional<Image?> Avatar { get; set; }
    }
}
