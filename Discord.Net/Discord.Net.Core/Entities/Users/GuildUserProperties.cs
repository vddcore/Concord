using System.Collections.Generic;

namespace Discord
{
    public class GuildUserProperties
    {
        public Optional<bool> Mute { get; set; }
        public Optional<bool> Deaf { get; set; }
        public Optional<string> Nickname { get; set; }
        public Optional<IEnumerable<IRole>> Roles { get; set; }
        public Optional<IEnumerable<ulong>> RoleIds { get; set; }
        public Optional<IVoiceChannel> Channel { get; set; }
        public Optional<ulong> ChannelId { get; set; } // TODO: v3 breaking change, change ChannelId to ulong? to allow for kicking users from voice
    }
}
