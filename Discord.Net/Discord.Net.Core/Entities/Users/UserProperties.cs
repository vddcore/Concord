using System;

namespace Discord
{
    [Flags]
    public enum UserProperties
    {
        None = 0,
        Staff = 1 << 0,
        Partner = 1 << 1,
        HypeSquadEvents = 1 << 2,
        BugHunterTierOne = 1 << 3,
        HypeSquadBravery = 1 << 6,
        HypeSquadBrilliance = 1 << 7,
        HypeSquadBalance = 1 << 8,
        EarlySupporter = 1 << 9,
        TeamUser = 1 << 10,
        System = 1 << 12,
        BugHunterTierTwo = 1 << 14
    }
}
