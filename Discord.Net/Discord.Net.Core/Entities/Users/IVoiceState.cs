namespace Discord
{
    public interface IVoiceState
    {
        bool IsDeafened { get; }
        bool IsMuted { get; }
        bool IsSelfDeafened { get; }
        bool IsSelfMuted { get; }
        bool IsSuppressed { get; }
        IVoiceChannel VoiceChannel { get; }
        string VoiceSessionId { get; }
        bool IsStreaming { get; }
    }
}
