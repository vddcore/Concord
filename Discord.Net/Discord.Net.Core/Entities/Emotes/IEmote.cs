namespace Discord
{
    public interface IEmote
    {
        string Name { get; }
    }
}
