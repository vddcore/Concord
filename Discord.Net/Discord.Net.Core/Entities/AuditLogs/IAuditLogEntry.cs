namespace Discord
{
    public interface IAuditLogEntry : ISnowflakeEntity
    {
        ActionType Action { get; }
        IAuditLogData Data { get; }
        
        IUser User { get; }
        string Reason { get; }
    }
}
