using System;
using System.Diagnostics;

namespace Discord
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public class CustomStatusGame : Game
    {
        internal CustomStatusGame() { }

        public IEmote Emote { get; internal set; }
        public DateTimeOffset CreatedAt { get; internal set; }
        
        public string State { get; internal set; }

        public override string ToString()
            => $"{Emote} {State}";

        private string DebuggerDisplay => $"{Name}";
    }
}
