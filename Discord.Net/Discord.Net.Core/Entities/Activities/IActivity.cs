namespace Discord
{
    public interface IActivity
    {
        string Name { get; }
        string Details { get; }
        
        ActivityType Type { get; }
        ActivityProperties Flags { get; }
    }
}
