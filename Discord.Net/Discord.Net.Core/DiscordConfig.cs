using System.Reflection;

namespace Discord
{
    public class DiscordConfig
    {
        public const int APIVersion = 6;
        public const int VoiceAPIVersion = 3;

        public static string Version { get; } =
            typeof(DiscordConfig).GetTypeInfo().Assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion ??
            typeof(DiscordConfig).GetTypeInfo().Assembly.GetName().Version.ToString(3) ??
            "Unknown";

        public static string UserAgent { get; } = $"DiscordBot (https://github.com/RogueException/Discord.Net, v{Version})";
        public static readonly string APIUrl = $"https://discordapp.com/api/v{APIVersion}/";
        
        public const string CDNUrl = "https://cdn.discordapp.com/";
        public const string InviteUrl = "https://discord.gg/";

        public const int DefaultRequestTimeout = 15000;
        public const int MaxMessageSize = 2000;
        public const int MaxMessagesPerBatch = 100;
        public const int MaxUsersPerBatch = 1000;
        public const int MaxGuildsPerBatch = 100;
        public const int MaxUserReactionsPerBatch = 100;
        public const int MaxAuditLogEntriesPerBatch = 100;

        public RetryMode DefaultRetryMode { get; set; } = RetryMode.AlwaysRetry;
        public LogSeverity LogLevel { get; set; } = LogSeverity.Info;
        internal bool DisplayInitialLog { get; set; } = true;

        public RateLimitPrecision RateLimitPrecision { get; set; } = RateLimitPrecision.Millisecond;
        public bool UseSystemClock { get; set; } = true;
    }
}
