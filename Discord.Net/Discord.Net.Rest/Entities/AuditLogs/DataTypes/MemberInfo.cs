namespace Discord.Rest
{
    public struct MemberInfo
    {
        public string Nickname { get; }
        public bool? Deaf { get; }
        public bool? Mute { get; }

        internal MemberInfo(string nick, bool? deaf, bool? mute)
        {
            Nickname = nick;
            Deaf = deaf;
            Mute = mute;
        }
    }
}
