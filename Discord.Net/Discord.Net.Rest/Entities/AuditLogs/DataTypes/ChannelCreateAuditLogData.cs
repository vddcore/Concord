using System.Collections.Generic;
using System.Linq;

using Model = Discord.API.AuditLog;
using EntryModel = Discord.API.AuditLogEntry;

namespace Discord.Rest
{
    public class ChannelCreateAuditLogData : IAuditLogData
    {
        public ulong ChannelId { get; }
        public string ChannelName { get; }
        public ChannelType ChannelType { get; }
        public int? SlowModeInterval { get; }
        public bool? IsNsfw { get; }
        public int? Bitrate { get; }
        public IReadOnlyCollection<Overwrite> Overwrites { get; }

        private ChannelCreateAuditLogData(ulong id, string name, ChannelType type, int? rateLimit, bool? nsfw, int? bitrate, IReadOnlyCollection<Overwrite> overwrites)
        {
            ChannelId = id;
            ChannelName = name;
            ChannelType = type;
            SlowModeInterval = rateLimit;
            IsNsfw = nsfw;
            Bitrate = bitrate;
            Overwrites = overwrites;
        }

        internal static ChannelCreateAuditLogData Create(BaseDiscordClient discord, Model log, EntryModel entry)
        {
            var changes = entry.Changes;
            var overwrites = new List<Overwrite>();

            var overwritesModel = changes.FirstOrDefault(x => x.ChangedProperty == "permission_overwrites");
            var typeModel = changes.FirstOrDefault(x => x.ChangedProperty == "type");
            var nameModel = changes.FirstOrDefault(x => x.ChangedProperty == "name");
            var rateLimitPerUserModel = changes.FirstOrDefault(x => x.ChangedProperty == "rate_limit_per_user");
            var nsfwModel = changes.FirstOrDefault(x => x.ChangedProperty == "nsfw");
            var bitrateModel = changes.FirstOrDefault(x => x.ChangedProperty == "bitrate");

            var type = typeModel.NewValue.ToObject<ChannelType>(discord.ApiClient.Serializer);
            var name = nameModel.NewValue.ToObject<string>(discord.ApiClient.Serializer);
            int? rateLimitPerUser = rateLimitPerUserModel?.NewValue?.ToObject<int>(discord.ApiClient.Serializer);
            bool? nsfw = nsfwModel?.NewValue?.ToObject<bool>(discord.ApiClient.Serializer);
            int? bitrate = bitrateModel?.NewValue?.ToObject<int>(discord.ApiClient.Serializer);

            foreach (var overwrite in overwritesModel.NewValue)
            {
                var deny = overwrite["deny"].ToObject<ulong>(discord.ApiClient.Serializer);
                var permType = overwrite["type"].ToObject<PermissionTarget>(discord.ApiClient.Serializer);
                var id = overwrite["id"].ToObject<ulong>(discord.ApiClient.Serializer);
                var allow = overwrite["allow"].ToObject<ulong>(discord.ApiClient.Serializer);

                overwrites.Add(new Overwrite(id, permType, new OverwritePermissions(allow, deny)));
            }

            return new ChannelCreateAuditLogData(entry.TargetId.Value, name, type, rateLimitPerUser, nsfw, bitrate, overwrites.ToReadOnlyCollection());
        }
    }
}
