﻿using System.Linq;

using EntryModel = Discord.API.AuditLogEntry;
using Model = Discord.API.AuditLog;

namespace Discord.Rest
{
    public class KickAuditLogData : IAuditLogData
    {
        public RestUser Target { get; }

        private KickAuditLogData(RestUser target)
        {
            Target = target;
        }

        internal static KickAuditLogData Create(BaseDiscordClient discord, Model log, EntryModel entry)
        {
            var userInfo = log.Users.FirstOrDefault(x => x.Id == entry.TargetId);
            return new KickAuditLogData(RestUser.Create(discord, userInfo));
        }
    }
}
