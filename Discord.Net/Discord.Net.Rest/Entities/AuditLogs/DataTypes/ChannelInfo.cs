namespace Discord.Rest
{
    public struct ChannelInfo
    {
        public string Name { get; }
        public string Topic { get; }
        public int? SlowModeInterval { get; }
        public bool? IsNsfw { get; }
        public int? Bitrate { get; }

        internal ChannelInfo(string name, string topic, int? rateLimit, bool? nsfw, int? bitrate)
        {
            Name = name;
            Topic = topic;
            SlowModeInterval = rateLimit;
            IsNsfw = nsfw;
            Bitrate = bitrate;
        }
    }
}
