namespace Discord.Rest
{
    public struct MemberRoleEditInfo
    {
        public string Name { get; }
        public ulong RoleId { get; }
        public bool Added { get; }

        internal MemberRoleEditInfo(string name, ulong roleId, bool added)
        {
            Name = name;
            RoleId = roleId;
            Added = added;
        }
    }
}
