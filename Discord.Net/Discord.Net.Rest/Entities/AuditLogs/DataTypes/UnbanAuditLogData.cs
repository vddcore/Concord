﻿using System.Linq;

using Model = Discord.API.AuditLog;
using EntryModel = Discord.API.AuditLogEntry;

namespace Discord.Rest
{
    public class UnbanAuditLogData : IAuditLogData
    {
        public IUser Target { get; }

        private UnbanAuditLogData(IUser target)
        {
            Target = target;
        }

        internal static UnbanAuditLogData Create(BaseDiscordClient discord, Model log, EntryModel entry)
        {
            var userInfo = log.Users.FirstOrDefault(x => x.Id == entry.TargetId);
            return new UnbanAuditLogData(RestUser.Create(discord, userInfo));
        }
    }
}
