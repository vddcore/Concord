using System;
using System.Diagnostics;
using Model = Discord.API.Channel;

namespace Discord.Rest
{
    [DebuggerDisplay(@"{DebuggerDisplay,nq}")]
    public class RestNewsChannel : RestTextChannel
    {
        internal RestNewsChannel(BaseDiscordClient discord, IGuild guild, ulong id)
            :base(discord, guild, id)
        {
        }
        internal new static RestNewsChannel Create(BaseDiscordClient discord, IGuild guild, Model model)
        {
            var entity = new RestNewsChannel(discord, guild, model.Id);
            entity.Update(model);
            return entity;
        }
        public override int SlowModeInterval => throw new NotSupportedException("News channels do not support Slow Mode.");
    }
}
