﻿namespace Discord.Net.Queue
{
    public enum ClientBucketType
    {
        Unbucketed = 0,
        SendEdit = 1
    }
}
